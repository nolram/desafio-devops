# Desafio DevOps

O desafio consiste em providenciar um serviço no Kubernetes que disponibilize o currículo em uma rota https e externa ao Kubernetes, o serviço também tem que contar com uma pipeline Jenkins para subir a aplicação no Ambiente.

## Estrutura de Pastas
```
.
├── ci-cd
│   ├── docker-compose.yaml
│   └── README.md
├── curriculo-app
│   ├── Dockerfile
│   ├── Jenkinsfile
│   ├── main.py
│   ├── properties.txt
│   ├── README.md
│   ├── requirements.txt
│   ├── static
│   │   ├── css
│   │   │   └── cover.css
│   │   ├── files
│   │   │   └── marlon_quadros.pdf
│   │   └── js
│   ├── templates
│   │   └── index.html
│   └── version.txt
├── docs
│   └── images
│       ├── Captura de tela *
├── kubefiles
│   ├── deployment.yml
│   ├── ingress.yml
│   ├── minikube
│   │   ├── minikube.cert
│   │   └── minikube.key
│   ├── README.md
│   └── service.yml
└── README.md
```

### ci-cd
Pasta contendo um docker-compose-yml para rodar um jenkins localmente, foi utilizado os plugins de Docker e Kubernetes do Jenkins (Junto com os plugins recomendados durante a instalação).

### curriculo-app
Serviço em Flask, desenvolvido para baixar o currículo e informações profissionais.

### docs
Documentação e prints da tela do desafio desenvolvido.

### kubefiles
Arquivos de configuração do Kubernetes para realizar o deploy do serviço curriculo-app no Kubernetes, também tem o processo utilizado para a criação de um certificado self-signed tls para ser utilizado no Ingress.