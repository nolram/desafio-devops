# Curriculo App
Serviço em Flask para mostrar as informações profissionais e realizar o Download do Currículo.

## Serviço
É possível rodar a aplicação usando o comando `python main.py` desde que as dependências do arquivo `requirements.txt` estejam instaladas em seu ambiente local.

## Dockerfile
Foi utilizado um imagem Alpine como base para diminuir o tamanho total da imagem. 

## Jenkinsfile
O Jenkinsfile foi desenvolvido de forma declarativa e separados por stages
1) Docker Build
2) Docker Push
3) Deploy