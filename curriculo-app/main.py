import os

from flask import Flask
from flask import render_template, jsonify

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/health')
def health():
    return jsonify({"health": "OK"})


if __name__ == "__main__":
    PORT = int(os.getenv("PORT", 8081))
    DEBUG = bool(os.getenv("DEBUG", True))
    
    app.run(host='0.0.0.0', port=PORT, debug=DEBUG)