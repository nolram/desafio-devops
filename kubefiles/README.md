# Comandos executados
```
minikube start
```

```
minikube addons enable ingress
```

https://kubernetes.github.io/ingress-nginx/user-guide/tls/
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout minikube -out minikube -subj "/CN=marlon.info/O=marlon.info"
```

```
kubectl create secret tls minikube-tls --key minikube.key --cert minikube.cert
```


https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/#create-an-ingress-resource


```
kubectl apply -f deployment.yml
kubectl apply -f service.yml
kubectl apply -f ingress.yml
```